import React, { useEffect, useRef, useState } from 'react';
import ky from 'ky';
import axios from 'axios';

type IApp = () => React.ReactElement;
const App: IApp = () => {
    const [query, setQuery] = useState('');
    const [data, setData] = useState([]);
    // useEffect(() => {
    //     fetch('https://jsonplaceholder.typicode.com/users')
    //         .then((response) => response.json())
    //         .then((json) => setData(json));
    // }, []);
    useEffect(() => {
        const getData = async () => {
            const response = await fetch('https://jsonplaceholder.typicode.com/users');
            const raw = await response.json();
            console.log('raw', raw);
            setData(raw);
        };
        getData();
    }, []);

    return (
        <div>
            Search:
            <input type="text" />
        </div>
    );
};
export default App;
